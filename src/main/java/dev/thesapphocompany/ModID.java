package dev.thesapphocompany;

import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModID implements ModInitializer {

	@Override
	public void onInitialize() {
		Constants.LOGGER.info("Hello Fabric world!");
	}
}